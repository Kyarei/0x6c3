document.addEventListener('keydown', registerKey);

const menuArea = document.getElementById("menu");
const gameArea = document.getElementById("ingame");
const songTitle = document.getElementById("nowPlaying");
const scoreDisplay = document.getElementById("score");
const lyricWindow = document.getElementById("lyrics");
const resultTitle = document.getElementById("resultTitle");
const resultGrade = document.getElementById("resultGrade");
const resultScore = document.getElementById("resultScore");
const resultAccuracy = document.getElementById("resultAccuracy");
const resultMaxChain = document.getElementById("resultMaxChain");

const tileImage = document.getElementById("tiles");

const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");
ctx.fillStyle = "green";
ctx.fillRect(69, 69, 420, 420);

const audioContext = new AudioContext();
const debugSound = new Audio("songs/debug.ogg");
debugSound.preload = 'auto';
debugSound.load();

let songList;
let songsById;

const screenWidth = 896;
const screenHeight = 720;
const tileHeight = 64;
const startY = -tileHeight; // starts higher than necessary in case drop is delayed
const catchY = screenHeight - 3 * tileHeight / 2;

const maxTileValue = 10000;
const maxTileValueBonus = 500;

let keyQueue = [];

const languageNames = {
  "art-x-v3-flicrinc": "Ŋarâþ Crîþ v9",
  "art-x-v3-arx0as": "Arka",
  "art-x-v3-zph0xl": "Shaleian",
};

const tilesByKey = { 'c': 0, 'k': 0, 'e': 1, 'n': 2, 'v': 3, 'o': 4, 's': 5 };

const nf = new Intl.NumberFormat('en-US');
const nfp = new Intl.NumberFormat('en-US', { style: 'percent', minimumFractionDigits: 1 });

function beautify(n) {
  return nf.format(n).replace("-", "−");
}
function beautifyPercent(n) {
  return nfp.format(n).replace("-", "−");
}

function createLookup(entries) {
  let lookup = {};
  for (let entry of entries) {
    lookup[entry.id] = entry;
  }
  return lookup;
}

function request(url, listener) {
  let oReq = new XMLHttpRequest();
  oReq.addEventListener("load", listener);
  oReq.open("GET", url);
  oReq.send();
}

function requestp(url) {
  console.log(url);
  return new Promise((resolve, reject) => {
    let oReq = new XMLHttpRequest();
    oReq.addEventListener("load", function () { resolve(this); });
    oReq.addEventListener("error", function () { reject(this); });
    oReq.open("GET", url);
    oReq.send();
  });
}

function setScreen(name) {
  for (let area of ["menu", "ingame", "results", "help"]) {
    document.getElementById(area).hidden = (name != area);
  }
}

function registerKey(ev) {
  keyQueue.push(ev.key);
}

function addMeta(dl, key, val) {
  if (!val) return;
  let div = document.createElement("div");
  let dt = document.createElement("dt");
  dt.innerText = key;
  let dd = document.createElement("dd");
  // dd.innerText = val;
  if (val instanceof Array) {
    for (let elem of val)
      dd.append(elem);
  } else {
    dd.append(val);
  }
  div.appendChild(dt);
  div.appendChild(dd);
  dl.appendChild(div);
}

function createSongInfo(song) {
  let root = document.createElement("div");
  root.setAttribute("class", "song");
  let title = document.createElement("h3");
  title.innerText = song.title;
  title.lang = song.titleLang;
  let meta = document.createElement("dl");
  meta.setAttribute("class", "songMeta");
  addMeta(meta, "Language", languageNames[song.lang] || song.lang);
  addMeta(meta, "Music", song.authors.music);
  addMeta(meta, "Lyrics", song.authors.lyrics);
  addMeta(meta, "Voice", song.authors.voice);
  if (song.links) {
    let links = [];
    for (let linkType in song.links) {
      let linkUrl = song.links[linkType];
      if (links.length > 0) {
        links.push(" ");
      }
      let link = document.createElement("a");
      link.href = linkUrl;
      link.innerText = `[${linkType}]`;
      links.push(link);
    }
    addMeta(meta, "Links", links);
  }
  let maps = document.createElement("div");
  maps.setAttribute("class", "maps");
  maps.append("Play: ");
  for (let mapInfo of song.maps) {
    let map = document.createElement("button");
    map.setAttribute("class", "playMap");
    map.innerText = mapInfo.label;
    let songId = song.id;
    let mapId = mapInfo.id;
    map.onclick = function () {
      playGame(songId, mapId);
    };
    maps.appendChild(map);
  }
  root.appendChild(title);
  root.appendChild(meta);
  root.appendChild(maps);
  return root;
}

async function playGame(songId, mapId) {
  console.log(`Starting game with song ${songId} and map ${mapId}`);
  let songInfo = songsById[songId];
  let mapInfo = songInfo.mapsById[mapId];
  let [track, duration] = await loadAudioIntoBuffer(songInfo.file);
  track.connect(audioContext.destination);

  let mapSource = (await requestp(mapInfo.file)).responseText;
  let map = parseMap(mapSource);
  let firstEventTime = Math.min(0, map.events[0].time);

  setScreen("ingame");
  songTitle.innerText = `${songInfo.title} – ${mapInfo.label}`;
  songTitle.lang = songInfo.titleLang;
  lyricWindow.innerText = "";
  lyricWindow.lang = songInfo.lang;
  canvas.focus();
  keyQueue = [];

  // TODO: investigate latency some more
  let now = performance.now();
  let songStartTime = audioContext.currentTime - firstEventTime;
  let gameState = {
    // startTime: now + Math.round(1000 * (-firstEventTime + audioContext.outputLatency)),
    startTime: now - Math.round(1000 * (firstEventTime)),
    fallingTiles: [],
    lyrics: "",
    evIndex: 0,
    splats: [],
    lastUpdate: firstEventTime,
    score: 0,
    consecutiveMisses: 0,
    consecutiveGoodMoves: 0,
    duration: duration,
    songStartTime: songStartTime,
    stats: {
      perfect: 0,
      excellent: 0,
      great: 0,
      good: 0,
      fair: 0,
      miss: 0,
      wrong: 0,
      maxChain: 0,
    }
  };

  track.start(songStartTime);
  console.log(`Song will start playing at ${songStartTime}`);
  update(now, gameState, map, audioContext);
}

/*
GAME STATE:
* startTime: time when the song started or will start (performance.now() timestamp)
* fallingTiles: array of {x, y, type} values
* lyrics: lyrics displayed so far
* evIndex: events processed so far
* splats: array of {x, y, age, text, color} values
* lastUpdate: time when last updated
* score: current score
* consecutiveMisses: number of consecutive misses
* pauseTime: time when game was paused (performance.now() timestamp; null if running)
*/

function update(timestamp, gameState, map) {
  checkKeys(gameState);
  let timeSinceStart = (performance.now() - gameState.startTime) / 1000;
  tick(timeSinceStart, gameState, map);
  render(gameState);
  if (timeSinceStart < gameState.duration + 2) {
    window.requestAnimationFrame((ts) => update(ts, gameState, map));
  } else {
    showResultScreen(gameState, map);
  }
}

function checkKeys(gameState) {
  for (let code of keyQueue) {
    let tileId = tilesByKey[code];
    if (tileId !== undefined) {
      // Get the tile closest to the collection zone
      let bestTileIdx = -1;
      let bestDist = Infinity;
      gameState.fallingTiles.forEach((tile, idx) => {
        if (tile.type != tileId) return;
        let dist = Math.abs(tile.y - catchY);
        if (dist < bestDist) {
          bestTileIdx = idx;
          bestDist = dist;
        }
      });
      if (bestDist <= tileHeight) {
        let tile = gameState.fallingTiles.splice(bestTileIdx, 1)[0];
        let msDist = 1000 * bestDist / tile.speed;
        let proportion;
        if (msDist <= 30) {
          proportion = 1;
        } else {
          proportion = 1 / (1 + 0.04 * (msDist - 20));
        }
        let value = Math.floor(proportion * (maxTileValue + maxTileValueBonus * gameState.consecutiveGoodMoves));
        gameState.score += value;
        let [statName, color] = appraise(proportion);
        gameState.splats.push({
          x: tile.x,
          y: tile.y,
          age: 0,
          lifespan: proportion == 1 ? 2 : 1,
          text: beautify(value),
          color: color,
        });
        gameState.consecutiveMisses = Math.max(0, gameState.consecutiveMisses - 4);
        if (proportion >= 0.5) {
          gameState.consecutiveGoodMoves++;
          gameState.stats.maxChain = Math.max(gameState.stats.maxChain, gameState.consecutiveGoodMoves);
          switch (gameState.consecutiveGoodMoves) {
            case 5:
              gameState.splats.push({
                x: screenWidth / 2,
                y: catchY - 160,
                age: 0,
                lifespan: 3,
                text: "Good!",
                color: [255, 255, 255],
              });
              break;
            case 10:
              gameState.splats.push({
                x: screenWidth / 2,
                y: catchY - 160,
                age: 0,
                lifespan: 3,
                text: "Excellent!",
                color: [255, 200, 255],
              });
              break;
            case 15:
              gameState.splats.push({
                x: screenWidth / 2,
                y: catchY - 160,
                age: 0,
                lifespan: 3,
                text: "Spectacular!",
                color: [255, 255, 200],
              });
              break;
            case 20:
              gameState.splats.push({
                x: screenWidth / 2,
                y: catchY - 160,
                age: 0,
                lifespan: 4,
                text: "Unbelievable!",
                color: ['rainbow', Math.random],
              });
          }
        } else {
          gameState.consecutiveGoodMoves = Math.max(0, gameState.consecutiveGoodMoves - 2);
        }
        gameState.stats[statName]++;
      } else {
        gameState.consecutiveMisses++;
        let penalty = 1000 * gameState.consecutiveMisses;
        gameState.score -= penalty;
        gameState.splats.push({
          x: 128 + 96 * tileId,
          y: catchY - tileHeight / 3,
          age: 0,
          lifespan: 1,
          text: beautify(-penalty),
          color: [255, 60, 60],
        });
        gameState.consecutiveGoodMoves = 0;
        gameState.stats.wrong++;
      }
    }
  }
  keyQueue = [];
}

function appraise(proportion) {
  if (proportion == 1) {
    return ["perfect", ['rainbow', Math.random()]];
  }
  if (proportion >= 0.9) {
    return ["excellent", [255, 255, 128]];
  }
  if (proportion >= 0.75) {
    return ["great", [255, 128, 160]];
  }
  if (proportion >= 0.6) {
    return ["good", [128, 160, 255]];
  }
  return ["fair", [160, 160, 160]];
}

function tick(timeSinceStart, gameState, map) {
  // Process map events – I don’t know why, but doing this first tends to improve timing.
  while (gameState.evIndex < map.events.length && map.events[gameState.evIndex].time <= timeSinceStart) {
    let ev = map.events[gameState.evIndex];
    if (ev.type == 'tile') {
      let ffTime = timeSinceStart - ev.time;
      let y = startY + ev.contents.speed * ffTime;
      gameState.fallingTiles.push({
        x: 128 + 80 * ev.contents.type,
        y: y,
        speed: ev.contents.speed,
        type: ev.contents.type,
        age: ffTime,
      });
      console.log(`Spawning a tile: type: ${ev.contents.type}, speed: ${ev.contents.speed}, ffTime = ${ffTime}, y = ${y}, current audio time; ${audioContext.currentTime}`);
    } else if (ev.type == 'lyrics') {
      gameState.lyrics += ev.contents;
      lyricWindow.innerText = gameState.lyrics;
      lyricWindow.scroll({ left: 0, top: lyricWindow.scrollHeight, behavior: "smooth" });
    }
    // console.log(`timeSinceStart = ${timeSinceStart}, audio prog = ${audio.currentTime}, delta = ${(audio.currentTime - timeSinceStart) * 1000}ms`);
    gameState.evIndex++;
  }
  // Update tile falls
  for (let tile of gameState.fallingTiles) {
    let oldY = tile.y;
    tile.y += tile.speed * (timeSinceStart - gameState.lastUpdate);
    tile.age += (timeSinceStart - gameState.lastUpdate);
    // if (oldY < catchY && tile.y >= catchY) {
    //   console.log(`Tile ${tile.type} crossed line at audio time ${(audio.currentTime - gameState.songStartTime) / 0.3125} E (oldY = ${oldY}, newY = ${tile.y}, age = ${tile.age})`);
    //   gameState.splats.push({
    //     x: tile.x,
    //     y: tile.y,
    //     age: 0,
    //     lifespan: 1,
    //     text: "Debug",
    //     color: [125, 255, 125],
    //   });
    //   debugSound.play();
    // }
    if (tile.y >= screenHeight + tileHeight) {
      gameState.stats.miss++;
      gameState.consecutiveGoodMoves = 0;
    }
  }
  // Remove tiles that have fallen too low
  gameState.fallingTiles = gameState.fallingTiles.filter(
    (tile) => tile.y < screenHeight + tileHeight);
  // Update splats
  for (let splat of gameState.splats) {
    splat.age += timeSinceStart - gameState.lastUpdate;
    splat.y -= 80 * (timeSinceStart - gameState.lastUpdate);
  }
  // Remove old splats
  gameState.splats = gameState.splats.filter((splat) => splat.age < splat.lifespan);
  gameState.lastUpdate = timeSinceStart;
}

function render(gameState) {
  ctx.globalCompositeOperation = 'destination-over';
  ctx.clearRect(0, 0, screenWidth, screenHeight);
  ctx.lineWidth = 2.0;
  ctx.strokeStyle = "#ffffff";
  ctx.beginPath();
  ctx.moveTo(0, catchY - tileHeight / 2);
  ctx.lineTo(screenWidth, catchY - tileHeight / 2);
  ctx.moveTo(0, catchY + tileHeight / 2);
  ctx.lineTo(screenWidth, catchY + tileHeight / 2);
  ctx.stroke();
  for (let tile of gameState.fallingTiles) {
    ctx.drawImage(tileImage, tileHeight * tile.type, 0, tileHeight, tileHeight, tile.x - tileHeight / 2, tile.y - tileHeight / 2, tileHeight, tileHeight);
  }
  ctx.font = "bold 48px Inter";
  ctx.textAlign = "center";
  ctx.textBaseline = "alphabetic";
  for (let splat of gameState.splats) {
    let alpha = 1 - (splat.age / splat.lifespan);
    if (splat.color[0] == 'rainbow') {
      let hue = 360 * ((1.8 * (splat.age + splat.color[1])) % 1);
      ctx.strokeStyle = `hsla(${hue}, 100%, 80%, ${alpha})`;
      ctx.fillStyle = `hsla(${hue}, 100%, 80%, ${0.5 * alpha})`;
    } else {
      ctx.strokeStyle = `rgba(${splat.color[0]}, ${splat.color[1]}, ${splat.color[2]}, ${alpha})`;
      ctx.fillStyle = `rgba(${splat.color[0]}, ${splat.color[1]}, ${splat.color[2]}, ${0.5 * alpha})`;
    }
    ctx.fillText(splat.text, splat.x, splat.y);
    ctx.strokeText(splat.text, splat.x, splat.y);
  }
  scoreDisplay.innerText = beautify(gameState.score);
}

function showResultScreen(gameState, map) {
  setScreen("results");
  let score = gameState.score;
  let maxScore = calculateTheoreticalScore(map);
  let grade = score / maxScore;
  resultScore.innerText = `Score: ${beautify(score)} / ${beautify(maxScore)} (${beautifyPercent(grade)})`;
  let [style, letter, titleText] = appraiseResults(grade);
  resultGrade.innerText = letter;
  resultGrade.className = style;
  resultTitle.innerHTML = titleText;
  let s = gameState.stats;
  let hits = s.perfect + s.excellent + s.great + s.good + s.fair;
  let total = hits + s.miss + s.wrong;
  resultAccuracy.innerText = `Accuracy: ${beautifyPercent(hits / total)}`;
  resultMaxChain.innerText = `Max. chain: ${beautify(s.maxChain)}`;
}

function appraiseResults(grade) {
  if (grade == 1) {
    return ["rankThorn", "Þ", "Perfect! You’ve obtained a <span class=\"rankThorn\">Þ</span> rank!"];
  } else if (grade >= 0.9) {
    return ["rankLbar", "Ł", "Spectacular! You’ve obtained an <span class=\"rankLbar\">Ł</span> rank!"];
  } else if (grade >= 0.8) {
    return ["rankCarth", "#", "Awesome! You’ve obtained a <span class=\"rankCarth\">#</span> rank!"];
  } else if (grade >= 0.7) {
    return ["rankSh", "Š", "You’ve obtained an <span class=\"rankSh\">Š</span> rank!"];
  } else if (grade >= 0.5) {
    return ["rankV", "V", "You’ve obtained a <span class=\"rankV\">V</span> rank."];
  } else if (grade >= 0.3) {
    return ["rankG", "G", "You’ve obtained a <span class=\"rankG\">G</span> rank."];
  } else if (grade >= 0) {
    return ["rankB", "B", "You’ve obtained a <span class=\"rankB\">B</span> rank."];
  } else {
    return ["rank8", "8", "All you get is an <span class=\"rank8\">8</span>. Better luck next time."];
  }
}

function returnToMenu() {
  setScreen("menu");
  songTitle.innerText = "";
}

function init() {
  request("songs.json", function () {
    songList = JSON.parse(this.responseText);
    let songMenu = document.getElementById("songs");
    for (let song of songList) {
      song.mapsById = createLookup(song.maps);
      songMenu.appendChild(createSongInfo(song));
    }
    songsById = createLookup(songList);
  });
}

const barAndBeatRegex = /^(\d+):(?:(\d+)|(\d+)\/(\d+)|(\d+)\+(\d+)\/(\d+))$/;

function parseBarAndBeat(text) {
  let matches = text.match(barAndBeatRegex);
  if (!matches) return null;
  if (matches[2]) return { bar: +matches[1], beat: [+matches[2], 1] };
  if (matches[3]) return { bar: +matches[1], beat: [+matches[3], +matches[4]] };
  return { bar: +matches[1], beat: [(+matches[5]) * (+matches[7]) + (+matches[6]), +matches[7]] };
}

function compareBarAndBeats(a, b) {
  if (a.bar < b.bar) return -1;
  if (a.bar > b.bar) return 1;
  if (a.beat == null && b.beat == null) return 0;
  if (a.beat == null) return -1;
  if (b.beat == null) return 1;
  let numA = a.beat[0] * b.beat[1];
  let numB = b.beat[0] * a.beat[1];
  return numA - numB;
}

function gcd(a, b) {
  if (a == 0) return b;
  if (b == 0) return a;
  if (a % 2 == 0 && b % 2 == 0) return 2 * gcd(a / 2, b / 2);
  if (a % 2 == 0) return gcd(a / 2, b);
  if (b % 2 == 0) return gcd(a, b / 2);
  return gcd(Math.abs(a - b), Math.min(a, b));
}

function simplifyRat(r) {
  let g = gcd(r[0], r[1]);
  return [r[0] / g, r[1] / g];
}

function addRat(a, b) {
  let g = gcd(a[1], b[1]);
  let d = a[1] * b[1] / g;
  let n = a[0] * (b[1] / g) + b[0] * (a[1] / g);
  return [n, d];
}

function parseMap(sourceText) {
  let lines = sourceText.split("\n");
  let events = [];
  let properties = {};
  for (let line of lines) {
    let res;
    if (line === "" || line.startsWith("#REM ")) continue;
    if ((res = line.match(/^\#tempo (\d+)(?:at (\d+))?$/))) {
      let when = res[2] ? +res[2] : 0;
      events.push({ type: 'tempo', time: { bar: when, beat: null }, contents: +res[1] });
    } else if ((res = line.match(/^\#time (\d+)(?:at (\d+))?$/))) {
      let when = res[2] ? +res[2] : 0;
      events.push({ type: 'timesig', time: { bar: when, beat: null }, contents: +res[1] });
    } else if ((res = line.match(/^\#timeWindow (\d+(?:\.\d+))(?:at (\d+))?$/))) {
      let when = res[2] ? +res[2] : 0;
      events.push({ type: 'timeWindow', time: { bar: when, beat: null }, contents: +res[1] });
    } else if ((res = line.match(/^tile (\S+)(( ([0-9,]+|\.))*)$/))) {
      let barAndBeat = parseBarAndBeat(res[1]);
      let tileGroups = res[2].substring(1).split(" ")
        .map((a) => {
          return a == '.' ? [] : a.split(",").map((a) => +a);
        });
      tileGroups.forEach((tileGroup, idx) => {
        let currentBarAndBeat = {
          bar: barAndBeat.bar,
          beat: simplifyRat([barAndBeat.beat[0] + idx, barAndBeat.beat[1]])
        }
        for (let tileType of tileGroup) {
          events.push({ type: 'tile', time: currentBarAndBeat, contents: tileType });
        }
      });
    } else if ((res = line.match(/^lyrics (\S+) (.+)$/))) {
      let barAndBeat = parseBarAndBeat(res[1]);
      let contents = JSON.parse(res[2]);
      if (typeof (contents) !== 'string') {
        throw new Error(`Expected string as lyrics; got ${contents}`);
      }
      events.push({ type: 'lyrics', time: barAndBeat, contents: contents });
    }
  }
  events.sort((a, b) => compareBarAndBeats(a.time, b.time));
  let eventsByTimestamp = [];
  let currentTempo = 1;
  let currentTime = 0;
  let currentTimeWindow = 2;
  let bar = 0;
  let timeElapsedAtBar = [0, 1];
  for (let pendingEvent of events) {
    let pendingEventTime = pendingEvent.time;
    if (pendingEvent.type == 'tempo') {
      // Fast forward to bar of tempo change
      let diff = pendingEventTime.bar - bar;
      let timeAdvanced = simplifyRat([60 * currentTime * diff, currentTempo]);
      bar = pendingEventTime.bar;
      timeElapsedAtBar = addRat(timeElapsedAtBar, timeAdvanced);
      currentTempo = pendingEvent.contents;
    } else if (pendingEvent.type == 'timesig') {
      // Fast forward to bar of tempo change
      let diff = pendingEventTime.bar - bar;
      let timeAdvanced = simplifyRat([60 * currentTime * diff, currentTempo]);
      bar = pendingEventTime.bar;
      timeElapsedAtBar = addRat(timeElapsedAtBar, timeAdvanced);
      currentTime = pendingEvent.contents;
    } else if (pendingEvent.type == 'timeWindow') {
      currentTimeWindow = pendingEvent.contents;
    } else {
      let barDiff = pendingEventTime.bar - bar;
      let timeAdvancedBars = simplifyRat([60 * currentTime * barDiff, currentTempo]);
      let timeAdvancedBeats = simplifyRat([60 * pendingEventTime.beat[0], currentTempo * pendingEventTime.beat[1]]);
      let timestamp = addRat(timeElapsedAtBar, addRat(timeAdvancedBars, timeAdvancedBeats));
      let newEvent = { type: pendingEvent.type, time: timestamp[0] / timestamp[1], contents: pendingEvent.contents };
      if (newEvent.type == 'tile') {
        // This event now reflects the tile drop instead of the tile hit
        newEvent.time -= currentTimeWindow;
        newEvent.contents = {
          type: pendingEvent.contents,
          speed: (catchY - startY) / currentTimeWindow,
        };
      }
      eventsByTimestamp.push(newEvent);
    }
  }
  eventsByTimestamp.sort((a, b) => a.time - b.time);
  console.log(eventsByTimestamp);
  return { events: eventsByTimestamp, properties: properties };
}

function calculateTheoreticalScore(map) {
  let nTiles = 0;
  for (let ev of map.events) {
    if (ev.type == 'tile') {
      ++nTiles;
    }
  }
  return calculateTheoreticalScoreFromTileCount(nTiles);
}

function calculateTheoreticalScoreFromTileCount(nTiles) {
  return nTiles * maxTileValue + maxTileValueBonus * nTiles * (nTiles - 1) / 2;
}

async function loadAudioIntoBuffer(url) {
  let source = audioContext.createBufferSource();
  let data = (await new Promise((resolve, reject) => {
    let oReq = new XMLHttpRequest();
    oReq.addEventListener("load", function () { resolve(this) });
    oReq.addEventListener("error", function () { reject(this) });
    oReq.responseType = 'arraybuffer';
    oReq.open("GET", url);
    oReq.send();
  })).response;
  let decodedData = await audioContext.decodeAudioData(data);
  source.buffer = decodedData;
  return [source, decodedData.duration];
}
