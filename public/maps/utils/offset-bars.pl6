#!/usr/bin/env raku

my $offset = @*ARGS[0];

for $*IN.lines {
  if /^ lyrics ' ' (\d+):(.*) $/ {
    say "lyrics {$0 + $offset}$1";
  } else {
    .say;
  }
}
